#!/usr/bin/env bats

setup() {
  DOCKER_IMAGE=${DOCKER_IMAGE:="test/firebase-distribute-testers-pipe"}
  #cp test/*.apk .
  #cp test/*.txt .

  #echo "Building image..."
  #docker build -t ${DOCKER_IMAGE}:test .
}

@test "Dummy test" {
#    run docker run \
#        -e APK="app-staging.apk" \
#        -e FIREBASE_CODE="${FIREBASE_CODE}" \
#        -e FIREBASE_TOKEN="${FIREBASE_TOKEN}" \
#        -e RELEASE_NOTES_FILE="release_notes.txt" \
#        -e RELEASE_NOTES="Correções de bugs e melhorias de desempenho" \
#        -e TESTERS="tester@gmail.com" \
#        -e TESTERS_FILE="testers.txt" \
#        -e TESTERS_GROUPS="Testers" \
#        -e TESTERS_GROUPS_FILE="groups.txt" \
#        -e DEBUG_DEPLOY="true" \
#        -v $(pwd):$(pwd) \
#        -w $(pwd) \
#        ${DOCKER_IMAGE}:test

    run docker build -t ${DOCKER_IMAGE}:test .

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 0 ]
}

