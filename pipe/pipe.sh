#!/usr/bin/env bash

source "$(dirname "$0")/common.sh"

enable_debug

info "iniciando Firebase Distribute Testers ..."

# PARAMETROS OBRIGATORIOS
FIREBASE_CODE=${FIREBASE_CODE:?'FIREBASE_CODE não informado.'}
FIREBASE_TOKEN=${FIREBASE_TOKEN:?'FIREBASE_TOKEN não informado.'}

debug FIREBASE-CODE: "${FIREBASE_CODE}"
debug FIREBASE-TOKEN: "${FIREBASE_TOKEN}"

TYPE=${TYPE:="debug"}
DIR=${FLAVOR:+"/${FLAVOR}"}
FLAVOR=${FLAVOR:+"-${FLAVOR}"}
APK=${APK:="app/build/outputs/apk${DIR}/${TYPE}/app${FLAVOR}-${TYPE}.apk"}

debug APK: "${APK}"

# PARAMETROS OPCIONAIS
debug RELEASE-NOTES: "${RELEASE_NOTES}"
debug RELEASE-NOTES-FILE: "${RELEASE_NOTES_FILE}"

debug TESTERS: "${TESTERS}"
debug TESTERS-FILE: "${TESTERS_FILE}"

debug TESTERS-GROUPS: "${TESTERS_GROUPS}"
debug TESTERS-GROUPS-FILE: "${TESTERS_GROUPS_FILE}"

if [ ! -f "${APK}" ]; 
then
  fail "apk não localizado!"
fi

if [ -n "${RELEASE_NOTES}" ]; 
then
  ARGS_DEPLOY="${ARGS_DEPLOY} --release-notes \"${RELEASE_NOTES}\" "
elif [ -f "${RELEASE_NOTES_FILE}" ] 
then
  ARGS_DEPLOY="${ARGS_DEPLOY} --release-notes-file \"${RELEASE_NOTES_FILE}\""
fi

if [ -n "${TESTERS}" ];
then
  ARGS_DEPLOY="${ARGS_DEPLOY} --testers \"${TESTERS}\""
elif [ -f "${TESTERS_FILE}" ]
then
  ARGS_DEPLOY="${ARGS_DEPLOY} --testers-file \"${TESTERS_FILE}\""
fi

if [ -n "${TESTERS_GROUPS}" ];
then
  ARGS_DEPLOY="${ARGS_DEPLOY} --groups \"${TESTERS_GROUPS}\""
elif [ -f "${TESTERS_GROUPS_FILE}" ]
then
  ARGS_DEPLOY="${ARGS_DEPLOY} --groups-file '${TESTERS_GROUPS_FILE}'"
fi

if [[ "${DEBUG_DEPLOY}" == "true" ]]; 
then
  ARGS_DEPLOY="${ARGS_DEPLOY} --debug"
fi

debug ARGS_DEPLOY: "${ARGS_DEPLOY}"

run firebase appdistribution:distribute "${APK}" \
      --app "${FIREBASE_CODE}" \
      ${ARGS_DEPLOY} \
      --token "${FIREBASE_TOKEN}"

if [ "${status}" -eq 0 ]; then
  success "Firebase Distribute Testers executado com sucesso!"
else
  fail "Firebase Distribute Testers falhou! :("
fi
