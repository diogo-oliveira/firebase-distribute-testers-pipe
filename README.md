# Bitbucket Pipelines Pipe: Firebase Distribute Testers

Este pipe executa distribui o aplicativo para testadores

## Definição de YAML

Adicione o seguinte trecho à seção de script do seu arquivo `bitbucket-pipelines.yml`:

```yaml
script:
  - pipe: diogo0liveira/firebase-distribute-testers-pipe:1.0.0
    variables:
      FIREBASE_CODE: "<string>"
      FIREBASE_TOKEN: "<string>"
      # APK: "<string>" # Optional
      # FLAVOR: "<string>" # Optional
      # TYPE: "<string>" # Optional
      # RELEASE_NOTES: "<string>" # Optional
      # RELEASE_NOTES_FILE: "<string>" # Optional
      # TESTERS: "<string>" # Optional
      # TESTERS_FILES: "<string>" # Optional
      # TESTERS_GROUPS: "<string>" # Optional
      # TESTERS_GROUPS_FILES: "<string>" # Optional
      # DEBUG_DEPLOY: "<boolean>" # Optional
      # DEBUG: "<boolean>" # Optional
```
## Variáveis

| Variável Uso          | Descrição                                                                                                         |
| --------------------- | ----------------------------------------------------------------------------------------------------------------- |
| FIREBASE_CODE (*)     | Código da aplicação do firebase.Você pode encontrar no console do Firebase, na página Configurações gerais        |
| FIREBASE_TOKEN (*)    | Token de autorização: <https://firebase.google.com/docs/cli#cli-ci-systems>                                       |
| APK                   | Artefado que será distribuido. Quando informado, tem prioridade sobre FLAVOR e TYPE respectivamente.              |
| FLAVOR                | Flavor que será utilizado. Quando informado, O diretório padrão é:(`app/build/outputs/apk/{flavor}/debug/app-{flavor}-debug.apk`).     
| TYPE                  | Bulid type que será utilizado. Quando informado, O diretório padrão é:(`app/build/outputs/apk/debug/app-{TYPE}.apk`).                                                                                  |
| RELEASE_NOTES         | Notas de versão desta compilação                                                                                  |
| RELEASE_NOTES_FILE    | Ou um arquivo com notas de versão desta compilação                                                                |
| TESTERS               | Email dos testadores que você deseja convidar, separado por virgura                                               |
| TESTERS_FILES         | Ou um arquivo com emails dos testadores que você deseja convidar, separado por virgura                            |
| TESTERS_GROUPS        | Grupos de testadores que você deseja convidar, serparados por virgula: <https://firebase.google.com/docs/app-distribution/manage-testers>                                                                                                            |
| TESTERS_GROUPS_FILES  | Ou um arquivo com grupos de testadores que você deseja convidar, como uma lista serparada por virgula :           |
| DEBUG_DEPLOY          | Ativar informações extras de depuração do Firebase. Padrão: `false`                                               |
| DEBUG                 | Ativar informações extras de depuração. Padrão: `false`                                                           |

_ (*) ​​= variável necessária._

## Pré-requisitos

## Exemplos

Exemplo básico:

```yaml
- step:
  name: INSTR. TESTS
  script:
    - pipe: docker://diogo0liveira/firebase-distribute-testers-pipe:1.0.0
      variables:
        FIREBASE_CODE: 1:283372088385:android:a24fc196cff8f451937e64
        FIREBASE_TOKEN: 1//0hqkqn6S1__6MCgYIARAAGBESNwF-L9IrilsER2iCrHzu7JmYsDJN5a1SNlnk7_qGlMH4TqzMC2AdyaAzH8tqi9aQIeKBDPrD828
        APK: 'app/build/outputs/apk/debug/app-debug.apk'
             
        RELEASE_NOTES: 'Correções de bugs e melhorias de desempenho'
        TESTERS_FILES: 'testers.txt'
```

## Apoio, suporte
Se você precisar de ajuda com este canal ou tiver um problema ou solicitação de recurso, informe-nos.
O tubo é mantido por diogo0liveira@hotmail.com.

Se você estiver relatando um problema, inclua:

- a versão do pipe
- logs e mensagens de erro relevantes
- Passos para reproduzir